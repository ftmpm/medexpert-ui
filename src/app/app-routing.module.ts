import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './core/components/layout/layout.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'promo',
    pathMatch: 'full'
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'about',
        loadChildren: './about/about.module#AboutModule'
      },
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'setting',
        loadChildren: './setting/setting.module#SettingModule'
      },
      {
        path: 'diagnostics',
        loadChildren: './diagnostics/diagnostics.module#DiagnosticsModule'
      },
      {
        path: 'catalog',
        loadChildren: './thesaurus/thesaurus.module#ThesaurusModule'
      },
    ]
  },
  {
    path: 'promo',
    loadChildren: './promo/promo.module#PromoModule'
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
