import { Component, OnInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-promo',
  templateUrl: './promo.component.html',
  styleUrls: ['./promo.component.scss']
})
export class PromoComponent implements OnInit {
  public swiperConfig: SwiperConfigInterface;

  /**
   * Constructor PromoComponent
   */
  public constructor() {
    this.swiperConfig = <SwiperConfigInterface> {
      direction: 'horizontal',
      autoplay: {
        delay: 13000,
      },
      slidesPerView: 1,
      keyboard: true,
      mousewheel: true,
      scrollbar: false,
      navigation: false,
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
        hideOnClick: false
      }
    };
  }

  public ngOnInit(): void {
  }
}
