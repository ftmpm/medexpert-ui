import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  /**
   * Constructor DashboardComponent
   */
  public constructor() {
  }

  public ngOnInit(): void {
  }

  public ngOnDestroy(): void {
  }
}

