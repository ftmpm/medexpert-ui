import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSnackBarModule,
  MatTabsModule
} from '@angular/material';

import { DiagnosisComponent } from './components/diagnosis/diagnosis.component';
import { DiagnosticsComponent } from './components/diagnostics/diagnostics.component';
import { DrugsComponent } from './components/drugs/drugs.component';
import { DiagnosticsRoutingModule } from './diagnostics-routing.module';
import { DiseasesComponent } from './components/diseases/diseases.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    DiagnosticsRoutingModule
  ],
  declarations: [DiagnosisComponent, DrugsComponent, DiagnosticsComponent, DiseasesComponent]
})
export class DiagnosticsModule {
}
