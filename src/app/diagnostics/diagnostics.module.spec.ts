import { DiagnosticsModule } from './diagnostics.module';

describe('DiagnosticsModule', () => {
  let diagnosticsModule: DiagnosticsModule;

  beforeEach(() => {
    diagnosticsModule = new DiagnosticsModule();
  });

  it('should create an instance', () => {
    expect(diagnosticsModule).toBeTruthy();
  });
});
