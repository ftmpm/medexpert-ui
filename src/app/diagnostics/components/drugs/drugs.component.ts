import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { DiagnosisInterface } from '../../../core/models/diagnosis';
import { Drug, DrugInterface } from '../../../core/models/drug';
import { DrugQueryInterface } from '../../../core/models/drug-query';
import { DataService } from '../../../core/services/data.service';
import { MedExpertService } from '../../../core/services/med-expert.service';

@Component({
  selector: 'app-drugs',
  templateUrl: './drugs.component.html',
  styleUrls: ['./drugs.component.scss']
})
export class DrugsComponent implements OnInit, OnDestroy {
  public options: string[];
  private subscription: Subscription;
  public diagnosisControl: FormControl;
  public filteredOptions: Observable<string[]>;
  public diagnosis: DiagnosisInterface;
  public submitted: boolean;
  public drugs: DrugInterface[];

  public constructor(private mes: MedExpertService) {
    this.subscription = new Subscription();
    this.diagnosisControl = new FormControl();
    this.diagnosis = DataService.getDiagnosis();
    this.drugs = [];
    this.submitted = false;
  }

  public ngOnInit() {
    this.subscription.add(this.mes.contraindications()
      .subscribe((res: any) => {
        this.submitted = false;
        this.options = res.body.collection.filter(item => this.diagnosis.contraindications.indexOf(item) < 0);
        this.setFilters();
      }, error => {
        this.submitted = false;
      })
    );
    this.load();
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private load(): void {
    this.submitted = true;
    this.subscription.add(this.mes.drugs(<DrugQueryInterface> {
        diseases: this.diagnosis.diseases,
        contraindications: this.diagnosis.contraindications
      }).subscribe((res: any) => {
      this.submitted = false;
        this.drugs = res.body.collection.map(item => Object.assign(new Drug(), item));
      }, error => {
        this.submitted = false;
      })
    );
  }

  private setFilters(): void {
    this.filteredOptions = this.diagnosisControl.valueChanges.pipe(
      startWith<string>(''),
      map(name => {
        return name ? this.filter(name) : this.options.slice();
      })
    );
  }

  public filter(name: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  public displayFn(name?: string): string | undefined {
    return name;
  }

  public onSubmit(event): void {
    const value: string = this.diagnosisControl.value;
    if (this.options.indexOf(value) >= 0) {
      if (this.diagnosis.contraindications.indexOf(value) < 0) {
        this.diagnosis.contraindications.push(value);
        const index: number = this.options.indexOf(value);
        if (index >= 0) {
          this.options.splice(index, 1);
          this.setFilters();
        }
      }
    } else {
      console.log('skip');
    }
    DataService.setDiagnosis(this.diagnosis);
    this.load();
    this.diagnosisControl.setValue(null);
  }

  public onRemoveContraindication(contraindication: string): void {
    const index: number = this.diagnosis.contraindications.indexOf(contraindication);
    if (index >= 0) {
      this.diagnosis.contraindications.splice(index, 1);
      DataService.setDiagnosis(this.diagnosis);
      this.load();
      this.options.push(contraindication);
      this.options.sort();
      this.setFilters();
    }
  }

  public getDesription(desription: string) {
    return desription.slice(0, 150);
  }
}
