import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { DiagnosisInterface } from '../../../core/models/diagnosis';
import { DataService } from '../../../core/services/data.service';
import { MedExpertService } from '../../../core/services/med-expert.service';

@Component({
  selector: 'app-diagnostics',
  templateUrl: './diagnosis.component.html',
  styleUrls: ['./diagnosis.component.scss']
})
export class DiagnosisComponent implements OnInit, OnDestroy {
  public options: string[];
  private subscription: Subscription;
  public diagnosisControl: FormControl;
  public filteredOptions: Observable<string[]>;
  public diagnosis: DiagnosisInterface;
  public diseases: Set<string>;
  public symptoms: Set<string>;
  public submitted: boolean;
  public prediction: string;

  public constructor(private mes: MedExpertService) {
    this.subscription = new Subscription();
    this.diagnosisControl = new FormControl();
    this.diagnosis = DataService.getDiagnosis();
    this.diseases = new Set<string>(this.diagnosis.diseases);
    this.symptoms = new Set<string>(this.diagnosis.symptoms);
    this.submitted = false;
  }

  public ngOnInit(): void {
    this.diagnostics();
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private load(): void {
    this.subscription.add(combineLatest(this.mes.symptoms(), this.mes.diseases()).subscribe((res: any) => {
      this.submitted = false;
      res[0].body.collection.forEach(disease => this.symptoms.add(disease));
      res[1].body.collection.forEach(disease => this.diseases.add(disease));
      this.options = [...res[0].body.collection, ...res[1].body.collection];
      this.symptoms = new Set<string>(res[0].body.collection || []);
      this.diseases = new Set<string>(res[1].body.collection || []);
      this.prediction = null;
      this.setFilters();
    }, error => {
      this.submitted = false;
    }));
  }

  private reload(): void {
    this.subscription.add(this.mes.diagnostics(this.diagnosis)
      .subscribe((res: any) => {
        this.submitted = false;
        const data = res.body;
        console.log(data);
        this.options = data.symptoms.filter(item => this.diagnosis.symptoms.indexOf(item) < 0) || [];
        this.diseases = new Set<string>(data.diseases || []);
        this.symptoms = new Set<string>(data.symptoms || []);
        this.prediction = data.prediction;
        this.setFilters();
      }, error => {
        this.submitted = false;
      })
    );
  }

  private setFilters(): void {
    this.filteredOptions = this.diagnosisControl.valueChanges.pipe(
      startWith<string>(''),
      map(name => {
        return name ? this.filter(name) : this.options.slice();
      })
    );
  }

  private diagnostics(): void {
    this.submitted = true;
    if (!this.diagnosis.symptoms.length && !this.diagnosis.diseases.length && !this.diagnosis.negativeSymptoms.length) {
      this.load();
    } else {
      this.reload();
    }
  }

  public filter(name: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  public displayFn(name?: string): string | undefined {
    return name;
  }

  public onSubmit(event): void {
    const value: string = this.diagnosisControl.value;
    if (this.diseases.has(value)) {
      if (this.diagnosis.diseases.indexOf(value) < 0) {
        this.diagnosis.diseases.push(value);
        this.diseases = new Set<string>([value]);
      }
      DataService.setDiagnosis(this.diagnosis);
    } else {
      if (this.symptoms.has(value)) {
        if (this.diagnosis.symptoms.indexOf(value) < 0) {
          this.diagnosis.symptoms.push(value);
        }
      } else {
        this.diagnosis.symptoms.push(value);
      }
      DataService.setDiagnosis(this.diagnosis);
      this.diagnostics();
    }
    this.diagnosisControl.setValue(null);
  }

  public onSelectSymptom(symptom: string): void {
    const index: number = this.diagnosis.symptoms.indexOf(symptom);
    if (index < 0) {
      this.diagnosis.symptoms.push(symptom);
      DataService.setDiagnosis(this.diagnosis);
      this.diagnostics();
    }
  }

  public onSelectNegativeSymptom(symptom: string): void {
    const index: number = this.diagnosis.symptoms.indexOf(symptom);
    if (index < 0) {
      this.diagnosis.negativeSymptoms.push(symptom);
      DataService.setDiagnosis(this.diagnosis);
      this.diagnostics();
    }
  }

  public onRemoveSymptom(symptom: string): void {
    const index: number = this.diagnosis.symptoms.indexOf(symptom);
    if (index >= 0) {
      this.diagnosis.symptoms.splice(index, 1);
      DataService.setDiagnosis(this.diagnosis);
      this.diagnostics();
    }
  }

  public onRemoveNegativeSymptom(symptom: string): void {
    const index: number = this.diagnosis.negativeSymptoms.indexOf(symptom);
    if (index >= 0) {
      this.diagnosis.negativeSymptoms.splice(index, 1);
      DataService.setDiagnosis(this.diagnosis);
      this.diagnostics();
    }
  }

  public onRemoveDisease(disease: string): void {
    const index: number = this.diagnosis.diseases.indexOf(disease);
    if (index >= 0) {
      this.diagnosis.diseases.splice(index, 1);
      DataService.setDiagnosis(this.diagnosis);
      this.diagnostics();
    }
  }

  public onSelectDisease(disease: string): void {
    if (this.diagnosis.diseases.indexOf(disease) < 0) {
      this.diagnosis.diseases.push(disease);
      DataService.setDiagnosis(this.diagnosis);
    }
  }

  public isSelectDisease(disease: string): boolean {
    return this.diagnosis.diseases.length && this.diagnosis.diseases.indexOf(disease) >= 0;
  }
}
