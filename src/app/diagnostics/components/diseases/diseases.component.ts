import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { DetailInterface } from '../../../core/models/detail';
import { DiagnosisInterface } from '../../../core/models/diagnosis';
import { Drug, DrugInterface } from '../../../core/models/drug';
import { DrugQueryInterface } from '../../../core/models/drug-query';
import { DataService } from '../../../core/services/data.service';
import { MedExpertService } from '../../../core/services/med-expert.service';

@Component({
  selector: 'app-diseases',
  templateUrl: './diseases.component.html',
  styleUrls: ['./diseases.component.scss']
})
export class DiseasesComponent implements OnInit, OnDestroy {
  public options: string[];
  private subscription: Subscription;
  public diagnosisControl: FormControl;
  public filteredOptions: Observable<string[]>;
  public diagnosis: DiagnosisInterface;
  public submitted: boolean;
  public diseases: string[];
  public drug: string;

  public constructor(private mes: MedExpertService) {
    this.subscription = new Subscription();
    this.diagnosisControl = new FormControl();
    this.diagnosis = DataService.getDiagnosis();
    this.diseases = [];
    this.submitted = false;
  }

  public ngOnInit() {
    this.subscription.add(this.mes.drugsAll()
      .subscribe((res: any) => {
        this.submitted = false;
        this.options = res.body.collection.map(item => item.name);
        this.setFilters();
      }, error => {
        this.submitted = false;
      })
    );
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private load(): void {
    this.submitted = true;
    this.subscription.add(this.mes.diseasesList(<DetailInterface> {
        name: this.drug
      }).subscribe((res: any) => {
        this.submitted = false;
        this.diseases = res.body.collection;
      }, error => {
        this.submitted = false;
      })
    );
  }

  private setFilters(): void {
    this.filteredOptions = this.diagnosisControl.valueChanges.pipe(
      startWith<string>(''),
      map(name => {
        return name ? this.filter(name) : this.options.slice();
      })
    );
  }

  public filter(name: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  public displayFn(name?: string): string | undefined {
    return name;
  }

  public onSubmit(event): void {
    this.drug = this.diagnosisControl.value;
    this.diagnosis.drug = this.drug;
    DataService.setDiagnosis(this.diagnosis);
    this.load();
    this.diagnosisControl.setValue(null);
  }

  public onRemoveDrug(): void {
    this.drug = null;
    this.diagnosis.drug = null;
    DataService.setDiagnosis(this.diagnosis);
  }
}
