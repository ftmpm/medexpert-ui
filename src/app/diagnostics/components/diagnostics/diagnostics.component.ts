import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diagnostics',
  templateUrl: './diagnostics.component.html',
  styleUrls: ['./diagnostics.component.scss']
})
export class DiagnosticsComponent implements OnInit {
  public navLinks: any[];

  public constructor() {
    this.navLinks = [
      {
        path: ['/', 'diagnostics', 'analyze'],
        label: 'Анализ'
      },
      {
        path: ['/', 'diagnostics', 'drugs'],
        label: 'Лечение'
      },
      {
        path: ['/', 'diagnostics', 'diseases'],
        label: 'Препараты'
      }
    ];
  }

  public ngOnInit(): void {
  }
}
