import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DiagnosisComponent } from './components/diagnosis/diagnosis.component';
import { DiagnosticsComponent } from './components/diagnostics/diagnostics.component';
import { DiseasesComponent } from './components/diseases/diseases.component';
import { DrugsComponent } from './components/drugs/drugs.component';

const routes: Routes = [
  {
    path: '',
    component: DiagnosticsComponent,
    children: [
      {
        path: '',
        redirectTo: 'analyze',
        pathMatch: 'full'
      },
      {
        path: 'analyze',
        component: DiagnosisComponent
      },
      {
        path: 'drugs',
        component: DrugsComponent
      },
      {
        path: 'diseases',
        component: DiseasesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiagnosticsRoutingModule {
}
