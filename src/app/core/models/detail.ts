import { Query, QueryInterface } from './query';
import { ResultInterface } from './result';

/**
 * DetailInterface
 */
export interface DetailInterface extends QueryInterface, ResultInterface {
  name: string;
}

/**
 * Detail
 */
export class Detail extends Query implements DetailInterface {
  public name: string;

  /**
   * Constructor Detail
   *
   * @param {DetailInterface} config
   */
  public constructor(config ?: DetailInterface) {
    super(config);
  }
}
