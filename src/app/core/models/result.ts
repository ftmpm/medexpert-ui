/**
 * ResultInterface
 */
export interface ResultInterface {
}

/**
 * Result
 */
export class Result implements ResultInterface {
  /**
   * Constructor Result
   * @param {ResultInterface} config
   */
  public constructor(config ?: ResultInterface) {
    Object.assign(this, config);
  }
}
