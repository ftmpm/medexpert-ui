/**
 * QueryInterface
 */
export interface QueryInterface {
}

/**
 * Query
 */
export class Query implements QueryInterface {
  /**
   * Constructor Query
   * @param {QueryInterface} config
   */
  public constructor(config ?: QueryInterface) {
    Object.assign(this, config);
  }
}
