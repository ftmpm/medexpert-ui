import { Query, QueryInterface } from './query';
import { ResultInterface } from './result';

/**
 * DiagnosisInterface
 */
export interface DiagnosisInterface extends QueryInterface, ResultInterface {
  symptoms: string[];
  negativeSymptoms ?: string[];
  contraindications: string[];
  diseases ?: string[];
  drugs ?: string[];
  age ?: number;
  sex ?: boolean;
  prediction ?: string;
  drug ?: string;
}

/**
 * Diagnosis
 */
export class Diagnosis extends Query implements DiagnosisInterface {
  public symptoms: string[] = [];
  public negativeSymptoms: string[] = [];
  public contraindications: string[] = [];
  public diseases: string[] = [];
  public drugs: string[] = [];
  public age: number = 0;
  public sex: boolean = false;
  public prediction: string;
  public drug: string;

  /**
   * Constructor Diagnosis
   *
   * @param {DiagnosisInterface} config
   */
  public constructor(config ?: DiagnosisInterface) {
    super(config);
  }
}
