import { ResultInterface } from './result';
import { Result } from './result';

/**
 * DetailInterface
 */
export interface DrugInterface extends ResultInterface {
  description: string;
  name: string;
  price: string;
  link: string;
  cure: string[];
  contraindications: string[];
  methodOfApplication: string;
  interaction: string;
}

/**
 * Detail
 */
export class Drug extends Result implements DrugInterface {
  public description: string;
  public name: string;
  public price: string;
  public link: string;
  public cure: string[];
  public contraindications: string[];
  public methodOfApplication: string;
  public interaction: string;

  /**
   * Constructor Drug
   *
   * @param {DrugQueryInterface} config
   */
  public constructor(config ?: DrugInterface) {
    super(config);
  }
}
