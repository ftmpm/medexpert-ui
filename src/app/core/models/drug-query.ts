import { Query, QueryInterface } from './query';

/**
 * DetailInterface
 */
export interface DrugQueryInterface extends QueryInterface {
  diseases: string[];
  contraindications: string[];
  age ?: number;
  sex ?: boolean;
}

/**
 * Detail
 */
export class DrugQuery extends Query implements DrugQueryInterface {
  public diseases: string[];
  public contraindications: string[];
  public age ?: number;
  public sex ?: boolean;

  /**
   * Constructor Drug
   *
   * @param {DrugQueryInterface} config
   */
  public constructor(config ?: DrugQueryInterface) {
    super(config);
  }
}
