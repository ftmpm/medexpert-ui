import { Component, OnInit } from '@angular/core';

import { Diagnosis } from '../../models/diagnosis';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  public opened: boolean;

  public constructor() {
    this.opened = false;
  }

  public ngOnInit(): void {
  }

  public onLogout(): void {
    DataService.setDiagnosis(new Diagnosis());
  }
}
