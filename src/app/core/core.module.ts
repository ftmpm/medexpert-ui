import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatIconModule,
  MatSidenavModule,
  MatToolbarModule
} from '@angular/material';

import { LayoutComponent } from './components/layout/layout.component';
import { CoreRoutingModule } from './core-routing.module';
import { ContentTypeInterceptor } from './interceptor/content-type.interceptor';
import { MedExpertService } from './services/med-expert.service';

const CORE_COMPONENTS = [
  LayoutComponent
];

const CORE_SERVICES = [
  MedExpertService
];

/**
 * @param parentModule
 * @param {string} moduleName
 */
export function throwIfAlreadyLoaded(parentModule: any, moduleName: string): void {
  if (parentModule) {
    throw new Error(`${moduleName} has already been loaded. Import Core modules in the AppModule only.`);
  }
}

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    CoreRoutingModule
  ],
  declarations: [
    ...CORE_COMPONENTS
  ],
  exports: [
    ...CORE_COMPONENTS
  ],
  providers: [
    ...CORE_SERVICES,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ContentTypeInterceptor,
      multi: true
    },
  ]
})
export class CoreModule {
  /**
   * Constructor CoreModule
   * @param {CoreModule} parentModule
   */
  public constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
