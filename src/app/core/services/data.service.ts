import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';
import { DiagnosisInterface } from '../models/diagnosis';
import { Diagnosis } from '../models/diagnosis';

export class DataService {
  public static readonly diagnosisId: string = `${environment.app.id}_diagnosis`;

  public static isDiagnosis(): boolean {
    return localStorage.getItem(DataService.diagnosisId) != null;
  }

  public static getDiagnosis(): DiagnosisInterface {
    let diagnosis: (string|null) = localStorage.getItem(DataService.diagnosisId);
    if (!diagnosis) {
      DataService.create();
      diagnosis = localStorage.getItem(DataService.diagnosisId);
    }

    return JSON.parse(diagnosis);
  }

  public static setDiagnosis(diagnosis: DiagnosisInterface): void {
    return localStorage.setItem(DataService.diagnosisId, JSON.stringify(diagnosis));
  }

  public static clear(): void {
    localStorage.removeItem(DataService.diagnosisId);
    DataService.create();
  }

  private static create(): void {
    DataService.setDiagnosis(new Diagnosis());
  }
}
