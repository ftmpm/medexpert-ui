import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { DetailInterface } from '../models/detail';
import { Diagnosis } from '../models/diagnosis';
import { DiagnosisInterface } from '../models/diagnosis';
import { DrugQueryInterface } from '../models/drug-query';

/**
 * MedExpertServiceInterface
 */
export interface MedExpertServiceInterface {
  /**
   * @param {Diagnosis} diagnosis
   * @returns {Diagnosis}
   */
  diagnostics(diagnosis: DiagnosisInterface): Observable<DiagnosisInterface>;

  /**
   * @param {DetailInterface} detail
   * @returns {DetailInterface}
   */
  detail(detail: DetailInterface): Observable<any>;

  /**
   * @param {DetailInterface} detail
   * @returns {DetailInterface}
   */
  diseasesList(detail: DetailInterface): Observable<any>;

  /**
   * @returns {Observable<string[]>}
   */
  symptoms(): Observable<any>;

  /**
   * @returns {Observable<string[]>}
   */
  contraindications(): Observable<any>;

  /**
   * @returns {Observable<string[]>}
   */
  drugs(drug: DrugQueryInterface): Observable<any>;

  /**
   * @returns {Observable<string[]>}
   */
  drugsAll(): Observable<any>;
}

@Injectable()
export class MedExpertService implements MedExpertServiceInterface {
  private readonly options: any;
  private readonly httpHeaders: HttpHeaders;
  private readonly url: string;

  /**
   * @param {HttpClient} httpClient
   */
  public constructor(private httpClient: HttpClient) {
    this.options = {headers: this.httpHeaders};
    if (environment.api.prefix) {
      this.url = `${environment.api.path}/${environment.api.prefix}`;
    } else {
      this.url = environment.api.path;
    }
    this.httpHeaders = new HttpHeaders();
  }

  public detail(detail: DetailInterface): Observable<any> {
    return this.post('details', detail);
  }

  public diagnostics(diagnosis: DiagnosisInterface): Observable<any> {
    return this.post('diagnostics', diagnosis);
  }

  public drugs(drug: DrugQueryInterface): Observable<any> {
    return this.post('drugs', drug);
  }

  public drugsAll(): Observable<any> {
    return this.get('drugs-all');
  }

  public contraindications(): Observable<string[]> {
    return this.get('contraindications');
  }

  public symptoms(): Observable<string[]> {
    return this.get('symptoms');
  }

  public diseases(): Observable<string[]> {
    return this.get('diseases');
  }

  public diseasesList(detail: DetailInterface): Observable<string[]> {
    return this.post('diseases-list', detail);
  }

  private get(path: string, search: any = null): Observable<any> {
    const url = `${this.url}/${path}`;
    let params = new HttpParams();
    for (const propKey in search) {
      if (search.hasOwnProperty(propKey) && search[propKey]) {
        params = params.append(propKey, search[propKey].toString());
      }
    }
    const options = {headers: this.httpHeaders, params: params};

    return this.httpClient.get<HttpResponse<any>>(url, options)
      .pipe(catchError(this.handleError()));
  }

  /**
   * @param {string} path
   * @param entity
   * @returns {Observable<any>}
   */
  private post(path: string, entity: any = null): Observable<any> {
    const url = `${this.url}/${path}`;

    return this.httpClient.post<HttpResponse<any>>(url, JSON.stringify(entity), this.options)
      .pipe(catchError(this.handleError())
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      // console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
