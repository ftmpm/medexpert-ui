import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSnackBarModule,
  MatTabsModule
} from '@angular/material';

import { CatalogComponent } from './components/catalog/catalog.component';
import { DiseaseComponent } from './components/disease/disease.component';
import { DrugComponent } from './components/drug/drug.component';
import { ThesaurusRoutingModule } from './thesaurus-routing.module';
import { DiseaseListComponent } from './components/disease-list/disease-list.component';
import { DrugListComponent } from './components/drug-list/drug-list.component';

@NgModule({
  imports: [
    CommonModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTabsModule,
    ThesaurusRoutingModule
  ],
  declarations: [
    CatalogComponent,
    DiseaseComponent,
    DrugComponent,
    DiseaseListComponent,
    DrugListComponent
  ]
})
export class ThesaurusModule {
}
