import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CatalogComponent } from './components/catalog/catalog.component';
import { DiseaseListComponent } from './components/disease-list/disease-list.component';
import { DiseaseComponent } from './components/disease/disease.component';
import { DrugListComponent } from './components/drug-list/drug-list.component';
import { DrugComponent } from './components/drug/drug.component';

const routes: Routes = [
  {
    path: '',
    component: CatalogComponent,
    children: [
      {
        path: '',
        redirectTo: 'drugs',
        pathMatch: 'full'
      },
      {
        path: 'diseases',
        component: DiseaseListComponent
      },
      {
        path: 'drugs',
        component: DrugListComponent
      },
      {
        path: 'disease/:name',
        component: DiseaseComponent
      },
      {
        path: 'drug/:name',
        component: DrugComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThesaurusRoutingModule {
}
