import { ThesaurusModule } from './thesaurus.module';

describe('ThesaurusModule', () => {
  let thesaurusModule: ThesaurusModule;

  beforeEach(() => {
    thesaurusModule = new ThesaurusModule();
  });

  it('should create an instance', () => {
    expect(thesaurusModule).toBeTruthy();
  });
});
