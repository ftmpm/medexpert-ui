import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DetailInterface } from '../../../core/models/detail';
import { Drug, DrugInterface } from '../../../core/models/drug';
import { MedExpertService } from '../../../core/services/med-expert.service';

@Component({
  selector: 'app-drug',
  templateUrl: './drug.component.html',
  styleUrls: ['./drug.component.scss']
})
export class DrugComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private submitted: boolean;
  public drug: DrugInterface;

  public constructor(private mes: MedExpertService,
                     private router: Router,
                     private route: ActivatedRoute) {
    this.subscription = new Subscription();
    this.drug = null;
    this.submitted = false;
  }

  public ngOnInit(): void {
    this.load();
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private load(): void {
    this.submitted = true;
    this.subscription.add(this.route.params
      .pipe(
        switchMap(params => {
          return this.mes.detail(<DetailInterface> {
            name: params.name
          });
        })
      ).subscribe(res => {
        this.drug = res.body;
      })
    );
  }

  public getDesription(desription: string = '') {
    return desription.slice(0, 150);
  }
}
