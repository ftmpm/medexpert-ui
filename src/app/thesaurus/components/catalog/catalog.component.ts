import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
  public navLinks: any[];

  public constructor() {
    this.navLinks = [
      {
        path: ['/', 'catalog', 'drugs'],
        label: 'Препарат'
      },
      {
        path: ['/', 'catalog', 'diseases'],
        label: 'Заболевание'
      }
    ];
  }

  public ngOnInit(): void {
  }
}
