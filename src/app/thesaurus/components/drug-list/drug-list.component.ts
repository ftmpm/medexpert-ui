import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { Drug, DrugInterface } from '../../../core/models/drug';
import { MedExpertService } from '../../../core/services/med-expert.service';

@Component({
  selector: 'app-drug-list',
  templateUrl: './drug-list.component.html',
  styleUrls: ['./drug-list.component.scss']
})
export class DrugListComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private submitted: boolean;
  public drugs: DrugInterface[];

  public constructor(private mes: MedExpertService) {
    this.subscription = new Subscription();
    this.drugs = [];
    this.submitted = false;
  }

  public ngOnInit(): void {
    this.load();
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private load(): void {
    this.submitted = true;
    this.subscription.add(this.mes.drugsAll()
      .subscribe((res: any) => {
        this.submitted = false;
        this.drugs = res.body.collection.map(item => Object.assign(new Drug(), item));
      }, error => {
        this.submitted = false;
      })
    );
  }

  public getDesription(desription: string = '') {
    return desription.slice(0, 150);
  }
}
