export const environment = {
  production: true,
  app: {
    id: 'drux.ftm.pm',
    path: 'https://drux.ftm.pm',
  },
  api: {
    prefix: 'api',
    path: 'https://medexpert.ftm.pm'
  },
};
